/*
NOTE: Do not change the name of this file
NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)

Q2. Create a new file with lipsum data (you can google and get this).
Do File Read and write data to another file
Delete the original file
Using promise chaining
*/
const fs = require('fs');
const path = require('path');
const dirPath = './json_files';

function Create2Files() {
    for (let i = 0; i < 2; i++) {
        const filename = `file_${i}.json`;
        const filepath = path.join(dirPath, filename);
        const data = {
            id: i,
            name: `Random Name ${i}`,
            value: Math.random() * 100
        };
        const jsonData = JSON.stringify(data, null, 2);
        fs.writeFile(filepath, jsonData, (err) => {
            if (err) {
                console.error(err);
            } else {
                console.log(`File ${filename} created`);
            }
            setTimeout(() => {
                fs.unlink(filepath, (err) => {
                    if (err) {
                        console.error(err);
                        return;
                    }
                    console.log(`File ${filepath} was deleted`);
                });
            }, 2000)
        });
    }
}

Create2Files()

function createAndDeleteOriginalFile() {
    return new Promise((resolve, reject) => {
        fs.readFile('lipsum.txt', "utf-8", (err, data) => {
            if (err) {
                reject(err)
            }
            resolve(data)
        })
    })
}

createAndDeleteOriginalFile()
    .then((data) => {
        fs.writeFile('duplicate.txt', data, (err) => {
            if (err) {
                throw new Error(err)
            }
        })
        console.log(`File duplicate.txt created`);

    })
    .then(() => {
        fs.unlink('lipsum.txt', (err) => {
            if (err) {
                console.error(err);
                return;
            }
            console.log(`File lipsum.txt was deleted`);
        });
    })
    .catch((error) => {
        console.error(error)
    })



function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(user, "User not found");
    }
}



function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
    console.log(activity)
    return new Promise((resolve, reject) => {
        fs.appendFile('logs.txt', `${new Date().toISOString()}  ${user}  ${activity}`, (err) => {
            if (err) {
                console.error(`Error writing to filenames.txt: ${err.message}`);
                reject("Log append failed")
            }
            resolve("Log append successful")
        })
    })
}

login("Tejeswar", 3)
    .then((user) => {
        logData(user, `Login success\n`)
            .then((msg) => {
                console.log(msg)
            }).catch((msg) => {
                console.log(msg)
            })
        getData()
            .then((data) => {
                logData(user, `Get data success\n`)
                    .then((msg) => {
                        console.log(msg)
                    }).catch((msg) => {
                        console.log(msg)
                    })
            })
            .catch(() => {
                logData(user, `Get data failure\n`)
                    .then((msg) => {
                        console.log(msg)
                    }).catch((msg) => {
                        console.log(msg)
                    })
            })
    })
    .catch((user) => {
        logData(user, `Login failure\n`)
            .then((msg) => {
                console.log(msg)
            }).catch((msg) => {
                console.log(msg)
            })

    })

/*
Q3.
Use appropriate methods to 
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.
    
    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.
    
*/